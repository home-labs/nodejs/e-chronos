import {
    CalculatorHelper,
    DatetimeFormattingHelper,
    TimeStateType
} from './index.mjs';


export class Time {

    // S.O.L.I.D. - Dependency inversion (IoC)
    constructor(
        private timeState: TimeStateType
    ) {

    }

    set milliseconds(value: number) {
        this.timeState.milliseconds = value;
    }

    set seconds(value: number) {
        this.timeState.seconds = value;
    }

    set minutes(value: number) {
        this.timeState.minutes = value;
    }

    set hours(value: number) {
        this.timeState.hours = value
    }

    resolveMilliseconds() {
        return DatetimeFormattingHelper
            .fillZeros(Math.abs(this.timeState.milliseconds || 0), 3);
    }

    resolveSeconds() {
        return DatetimeFormattingHelper
            .fillZeros(Math.abs(this.timeState.seconds || 0), 2);
    }

    resolveMinutes() {
        return DatetimeFormattingHelper
            .fillZeros(Math.abs(this.timeState.minutes || 0), 2);
    }

    resolveHours() {
        return DatetimeFormattingHelper
            .fillZeros(Math.abs(this.timeState.hours || 0), 2);
    }

    addMilliseconds(value: number) {

        let total: number = this.timeState.milliseconds! + value;

        this.milliseconds = CalculatorHelper.calculateMilliseconds(total);

        this.addSeconds(CalculatorHelper.calculateSecondsContainedInMilliseconds(total));
    }

    addSeconds(value: number) {

        let total: number = this.timeState.seconds! + value;

        this.seconds = CalculatorHelper.calculateSeconds(total);

        this.addMinutes(CalculatorHelper.calculateMinutesContainedInSeconds(total));
    }

    addMinutes(value: number) {

        let total: number = this.timeState.minutes! + value;

        this.minutes = CalculatorHelper.calculateMinutes(total);

        this.addHours(CalculatorHelper.calculateHoursContainedInMinutes(total));
    }

    addHours(value: number) {
        this.hours = this.timeState.hours! + value;
    }

}
