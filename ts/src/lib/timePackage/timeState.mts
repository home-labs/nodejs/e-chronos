import { AccessorsHelper } from '@cyberjs.on/design-protocools/prototype';

import { TimeStateType } from './index.mjs';


export class TimeState implements TimeStateType {

    hours!: number;

    minutes!: number;

    seconds!: number;

    #_hours: number;

    #_minutes: number;

    #_seconds: number;

    #_milliseconds: number;

    constructor(timeState: TimeStateType) {

        this.#_hours = timeState.hours || 0;

        this.#_minutes = timeState.minutes || 0;

        this.#_seconds = timeState.seconds || 0;

        this.#_milliseconds = timeState.milliseconds || 0;

        AccessorsHelper.define4(this, [
            {
                name: 'hours',
                getter: () => {
                    return this.#_hours
                },
                setter: (value: number) => {
                    this.#_hours = value;
                }
            },
            {
                name: 'minutes',
                getter: () => {
                    return this.#_minutes
                },
                setter: (value: number) => {
                    this.#_minutes = value;
                }
            },
            {
                name: 'seconds',
                getter: () => {
                    return this.#_seconds
                },
                setter: (value: number) => {
                    this.#_seconds = value;
                }
            },
            {
                name: 'milliseconds',
                getter: () => {
                    return this.#_milliseconds
                },
                setter: (value: number) => {
                    this.#_milliseconds = value;
                }
            }
        ]);
    }

}
