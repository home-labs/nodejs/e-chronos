import { Originator } from '@cyberjs.on/design-protocools/memento';

import { TimeStateType } from '../index.mjs';
import { TimeMemento } from './timeMemento.mjs';


export class TimeOriginator implements Originator<TimeStateType> {


    constructor(
        private timeState: TimeStateType
    ) {

    }

    createMemento() {
        /**
         *
         * {... } tem o mesmo efeito que Object.assign({}, this.timeState), contudo propriedades não-enumeráveis não são copiadas, como é o caso de getters and
         * setters declarados de forma convencional, isto é, como functions.
         */
        return new TimeMemento({...this.timeState});
    }

    restoreMemento(memento: TimeMemento) {
        Object.assign(this.timeState, memento.state);
    }

}
