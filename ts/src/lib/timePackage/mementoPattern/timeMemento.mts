import { Memorable } from '@cyberjs.on/design-protocools/memento';

import { TimeStateType } from '../index.mjs';


export class TimeMemento implements Memorable<TimeStateType> {

    constructor(
        private timeState: TimeStateType
    ) { }

    get state() {
        return this.timeState;
    }

}
