export interface TimeStateType {

    hours: number;

    minutes: number;

    seconds: number;

    milliseconds?: number;

}
