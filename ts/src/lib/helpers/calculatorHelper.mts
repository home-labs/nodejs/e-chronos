import { TimeStateType } from '../index.mjs';


export class CalculatorHelper {

    /**
     *
     * @todo add support to elapsed milliseconds
     */
    static calculateElapsedTime(
        originalTimeState: TimeStateType,
        currentTime: TimeStateType
    ): TimeStateType {

        const calculatedMilliseconds = this.calculateMilliseconds(originalTimeState.milliseconds!);

        const calculatedSeconds = this.calculateSeconds(originalTimeState.seconds!);

        const calculatedMinutes = this.calculateMinutes(originalTimeState.minutes!);

        const elapsedResolvedMilliseconds = this.calculateMilliseconds(currentTime.milliseconds!);

        const elapsedResolvedSeconds = this.calculateSeconds(currentTime.seconds!);

        const elapsedResolvedMinutes = this.calculateMinutes(currentTime.minutes!);

        const elapsedMilliseconds = elapsedResolvedMilliseconds - calculatedMilliseconds;

        const elapsedSeconds = elapsedResolvedSeconds - calculatedSeconds;

        const elapsedMinutes = elapsedResolvedMinutes - calculatedMinutes;

        const elapsedHours = currentTime.hours! - originalTimeState.hours!;

        let realElapsedMilliseconds = elapsedMilliseconds;

        let realElapsedSeconds = elapsedSeconds;

        let realElapsedMinutes = elapsedMinutes;

        let realElapsedHours = elapsedHours;

        if (elapsedMilliseconds < 0) {
            realElapsedMilliseconds = 1000 - Math.abs(elapsedMilliseconds);
        }

        if (elapsedSeconds < 0) {
            realElapsedSeconds = 60 - Math.abs(elapsedSeconds);
        }

        if (elapsedMinutes < 0) {
            realElapsedMinutes = 60 - Math.abs(elapsedMinutes);
        }

        if (elapsedHours < 0) {
            realElapsedHours = 24 - Math.abs(elapsedHours);
        }

        return {
            hours: realElapsedHours,
            minutes: realElapsedMinutes,
            seconds: realElapsedSeconds,
            milliseconds: realElapsedMilliseconds
        };

    }

    static calculateMilliseconds(value: number) {
        return value - (this.calculateSecondsContainedInMilliseconds(value) * 1000);
    }

    static calculateSeconds(value: number) {
        return value - (this.calculateMinutesContainedInSeconds(value) * 60);
    }

    static calculateMinutes(value: number) {
        return value - (this.calculateHoursContainedInMinutes(value) * 60);
    }

    static calculateSecondsContainedInMilliseconds(milliseconds = 0) {
        return Math.trunc(milliseconds / 1000);
    }

    static calculateMinutesContainedInSeconds(seconds = 0) {
        return Math.trunc(seconds / 60);
    }

    static calculateHoursContainedInMinutes(minutes = 0) {
        return Math.trunc(minutes / 60);
    }

}
