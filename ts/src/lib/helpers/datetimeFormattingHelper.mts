export class DatetimeFormattingHelper {

    static #ISO8601Pattern = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})(?:\.(\d{3}))?(([+-]{1}\d{2}):?(\d{2})|Z)$/;

    static #timeZonePattern = /(?:([+-]{1}\d{2}):?(\d{2})|Z)/;

    static separateCombinedISO8601(stringDatetime: string, timeZone?: string) {

        if (!this.#validateISO8601DateFormat(stringDatetime)) {
            throw new Error (`The ${stringDatetime} passed isn't in ISO 8601 format.`);
        }

        const datePieces = stringDatetime.match(this.#ISO8601Pattern)!;

        const year = parseInt(datePieces[1], 10);

        const month = parseInt(datePieces[2], 10) - 1;

        const day = parseInt(datePieces[3], 10);

        const hours = parseInt(datePieces[4], 10);

        const minutes = parseInt(datePieces[5], 10);

        const seconds = parseInt(datePieces[6], 10);

        const milliseconds = parseInt(datePieces[7] || '0', 10);

        const datetimeZone = datePieces[8];

        let datetimeZoneHours = 0;

        let datetimeZoneMinutes = 0;

        let timeZoneHours = 0;

        let timeZoneMinutes = 0;

        let timeZoneHoursDifference = 0;

        let timeZoneMinutesDifference = 0;

        let timeZonePieces;

        let UTCDate;

        let date;

        if (timeZone) {
            if (!this.#validateTimeZoneFormat(timeZone)) {
                throw new Error (`The ${timeZone} passed isn't valid.`);
            }
        } else {
            timeZone = datetimeZone;
        }

        if (datetimeZone !== timeZone) {

            if (!this.#isInUTCFormat(datetimeZone)) {
                datetimeZoneHours = parseInt(datePieces[10], 10);
                datetimeZoneMinutes = parseInt(datePieces[11], 10);
            }

            timeZonePieces = timeZone.match(this.#timeZonePattern)!;

            if (!this.#isInUTCFormat(timeZone)) {
                timeZoneHours = parseInt(timeZonePieces[1], 10);
                timeZoneMinutes = parseInt(timeZonePieces[2], 10);
            }

            if (datetimeZoneHours > timeZoneHours) {
                timeZoneHoursDifference = timeZoneHours - datetimeZoneHours;
            } else {
                timeZoneHoursDifference = Math.abs(datetimeZoneHours - timeZoneHours);
            }

            if (datetimeZoneMinutes > timeZoneMinutes) {
                timeZoneMinutesDifference = datetimeZoneMinutes - timeZoneMinutes;
            } else {
                timeZoneMinutesDifference = Math.abs(datetimeZoneMinutes - timeZoneMinutes);
            }

        }

        UTCDate = Date.UTC(
            year,
            month,
            day,
            hours + timeZoneHoursDifference,
            minutes + timeZoneMinutesDifference,
            seconds,
            milliseconds
        );

        date = new Date(UTCDate);

        return date.toISOString()
            .replace(/T/, ' ')
            .replace(this.#timeZonePattern, '');

    }

    static fillZeros(digitsCount: number): string;
    static fillZeros(value: number, digitsCount: number): string;
    static fillZeros(value: number, digitsCount?: number): string {

        let valueArg = value;

        let digitsCountArg = arguments[1];

        let resolvedValue = '';

        let valueIsNegative = false;

        if (digitsCountArg) {

            if (valueArg < 0) {
                valueIsNegative = true;
            }

            resolvedValue = Math.abs(valueArg).toString();
        } else {
            digitsCount = valueArg;
        }

        for (let i = 0; resolvedValue.length < digitsCount!; i++) {
            resolvedValue = `0${resolvedValue}`;
        }

        if (valueIsNegative) {
            return `-${resolvedValue}`;
        }

        return resolvedValue;
    }

    static #isInUTCFormat(stringDatetime: string) {
        return stringDatetime.match(/Z$/);
    }

    static #validateTimeZoneFormat(timeZone: string) {
        return this.#timeZonePattern.test(timeZone);
    }

    static #validateISO8601DateFormat(stringDatetime: string) {
        return this.#ISO8601Pattern.test(stringDatetime);
    }

}
