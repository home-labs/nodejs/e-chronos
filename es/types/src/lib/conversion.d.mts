import { Time } from './index.mjs';
export declare class Conversion {
    private time;
    constructor(time: Time);
    asMilliseconds(): number;
    asSeconds(): number;
    asMinutes(): number;
}
