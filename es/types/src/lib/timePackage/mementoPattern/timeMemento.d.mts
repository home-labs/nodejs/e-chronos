import { Memorable } from '@cyberjs.on/design-protocools/memento';
import { TimeStateType } from '../index.mjs';
export declare class TimeMemento implements Memorable<TimeStateType> {
    private timeState;
    constructor(timeState: TimeStateType);
    get state(): TimeStateType;
}
