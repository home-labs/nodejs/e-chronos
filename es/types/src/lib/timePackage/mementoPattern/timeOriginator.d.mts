import { Originator } from '@cyberjs.on/design-protocools/memento';
import { TimeStateType } from '../index.mjs';
import { TimeMemento } from './timeMemento.mjs';
export declare class TimeOriginator implements Originator<TimeStateType> {
    private timeState;
    constructor(timeState: TimeStateType);
    createMemento(): TimeMemento;
    restoreMemento(memento: TimeMemento): void;
}
