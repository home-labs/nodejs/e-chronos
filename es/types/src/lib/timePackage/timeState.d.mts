import { TimeStateType } from './index.mjs';
export declare class TimeState implements TimeStateType {
    #private;
    hours: number;
    minutes: number;
    seconds: number;
    constructor(timeState: TimeStateType);
}
