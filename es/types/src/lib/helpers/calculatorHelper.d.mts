import { TimeStateType } from '../index.mjs';
export declare class CalculatorHelper {
    static calculateElapsedTime(originalTimeState: TimeStateType, currentTime: TimeStateType): TimeStateType;
    static calculateMilliseconds(value: number): number;
    static calculateSeconds(value: number): number;
    static calculateMinutes(value: number): number;
    static calculateSecondsContainedInMilliseconds(milliseconds?: number): number;
    static calculateMinutesContainedInSeconds(seconds?: number): number;
    static calculateHoursContainedInMinutes(minutes?: number): number;
}
