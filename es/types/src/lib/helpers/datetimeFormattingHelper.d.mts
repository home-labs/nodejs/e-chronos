export declare class DatetimeFormattingHelper {
    #private;
    static separateCombinedISO8601(stringDatetime: string, timeZone?: string): string;
    static fillZeros(digitsCount: number): string;
    static fillZeros(value: number, digitsCount: number): string;
}
