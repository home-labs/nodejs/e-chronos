import { TimeStateType } from './index.mjs';
export declare class Time {
    private timeState;
    constructor(timeState: TimeStateType);
    set milliseconds(value: number);
    set seconds(value: number);
    set minutes(value: number);
    set hours(value: number);
    resolveMilliseconds(): string;
    resolveSeconds(): string;
    resolveMinutes(): string;
    resolveHours(): string;
    addMilliseconds(value: number): void;
    addSeconds(value: number): void;
    addMinutes(value: number): void;
    addHours(value: number): void;
}
