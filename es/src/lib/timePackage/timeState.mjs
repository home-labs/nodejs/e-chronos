import { AccessorsHelper } from '@cyberjs.on/design-protocools/prototype';
export class TimeState {
    hours;
    minutes;
    seconds;
    #_hours;
    #_minutes;
    #_seconds;
    #_milliseconds;
    constructor(timeState) {
        this.#_hours = timeState.hours || 0;
        this.#_minutes = timeState.minutes || 0;
        this.#_seconds = timeState.seconds || 0;
        this.#_milliseconds = timeState.milliseconds || 0;
        AccessorsHelper.define4(this, [
            {
                name: 'hours',
                getter: () => {
                    return this.#_hours;
                },
                setter: (value) => {
                    this.#_hours = value;
                }
            },
            {
                name: 'minutes',
                getter: () => {
                    return this.#_minutes;
                },
                setter: (value) => {
                    this.#_minutes = value;
                }
            },
            {
                name: 'seconds',
                getter: () => {
                    return this.#_seconds;
                },
                setter: (value) => {
                    this.#_seconds = value;
                }
            },
            {
                name: 'milliseconds',
                getter: () => {
                    return this.#_milliseconds;
                },
                setter: (value) => {
                    this.#_milliseconds = value;
                }
            }
        ]);
    }
}
//# sourceMappingURL=timeState.mjs.map