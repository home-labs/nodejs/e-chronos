export class TimeMemento {
    timeState;
    constructor(timeState) {
        this.timeState = timeState;
    }
    get state() {
        return this.timeState;
    }
}
//# sourceMappingURL=timeMemento.mjs.map