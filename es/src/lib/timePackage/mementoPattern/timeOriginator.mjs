import { TimeMemento } from './timeMemento.mjs';
export class TimeOriginator {
    timeState;
    constructor(timeState) {
        this.timeState = timeState;
    }
    createMemento() {
        return new TimeMemento({ ...this.timeState });
    }
    restoreMemento(memento) {
        Object.assign(this.timeState, memento.state);
    }
}
//# sourceMappingURL=timeOriginator.mjs.map