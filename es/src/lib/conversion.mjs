export class Conversion {
    time;
    constructor(time) {
        this.time = time;
    }
    asMilliseconds() {
        const milliseconds = parseInt(this.time.resolveMilliseconds());
        return (this.asSeconds() * 1000) + milliseconds;
    }
    asSeconds() {
        const seconds = parseInt(this.time.resolveSeconds());
        return (this.asMinutes() * 60) + seconds;
    }
    asMinutes() {
        const minutes = parseInt(this.time.resolveMinutes());
        const hours = parseInt(this.time.resolveHours());
        return (hours * 60) + minutes;
    }
}
//# sourceMappingURL=conversion.mjs.map