import { CalculatorHelper, DatetimeFormattingHelper } from './index.mjs';
export class Time {
    timeState;
    constructor(timeState) {
        this.timeState = timeState;
    }
    set milliseconds(value) {
        this.timeState.milliseconds = value;
    }
    set seconds(value) {
        this.timeState.seconds = value;
    }
    set minutes(value) {
        this.timeState.minutes = value;
    }
    set hours(value) {
        this.timeState.hours = value;
    }
    resolveMilliseconds() {
        return DatetimeFormattingHelper
            .fillZeros(Math.abs(this.timeState.milliseconds || 0), 3);
    }
    resolveSeconds() {
        return DatetimeFormattingHelper
            .fillZeros(Math.abs(this.timeState.seconds || 0), 2);
    }
    resolveMinutes() {
        return DatetimeFormattingHelper
            .fillZeros(Math.abs(this.timeState.minutes || 0), 2);
    }
    resolveHours() {
        return DatetimeFormattingHelper
            .fillZeros(Math.abs(this.timeState.hours || 0), 2);
    }
    addMilliseconds(value) {
        let total = this.timeState.milliseconds + value;
        this.milliseconds = CalculatorHelper.calculateMilliseconds(total);
        this.addSeconds(CalculatorHelper.calculateSecondsContainedInMilliseconds(total));
    }
    addSeconds(value) {
        let total = this.timeState.seconds + value;
        this.seconds = CalculatorHelper.calculateSeconds(total);
        this.addMinutes(CalculatorHelper.calculateMinutesContainedInSeconds(total));
    }
    addMinutes(value) {
        let total = this.timeState.minutes + value;
        this.minutes = CalculatorHelper.calculateMinutes(total);
        this.addHours(CalculatorHelper.calculateHoursContainedInMinutes(total));
    }
    addHours(value) {
        this.hours = this.timeState.hours + value;
    }
}
//# sourceMappingURL=time.mjs.map