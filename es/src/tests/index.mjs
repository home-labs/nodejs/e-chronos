import { Storage } from '@cyberjs.on/design-protocools/memento';
import { CalculatorHelper, Conversion, Time, TimeOriginator, TimeState } from '../index.mjs';
class Client {
    time;
    #timeState;
    #storage;
    constructor() {
        const timeState = {
            hours: 1,
            minutes: 59,
            seconds: 59,
            milliseconds: 998
        };
        this.#timeState = new TimeState(timeState);
        this.time = new Time(this.#timeState);
        this.#storage = new Storage(new TimeOriginator(this.#timeState));
    }
    ofConversion() {
        const conversor = new Conversion(this.time);
        console.log('\n');
        console.log('in minutes: ', conversor.asMinutes());
        console.log('in seconds: ', conversor.asSeconds());
        console.log('in milliseconds: ', conversor.asMilliseconds());
    }
    ofSum() {
        this.#storage.storeNewState();
        console.log('\n2 milliseconds has been added...\n');
        this.time.addMilliseconds(2);
        console.log(`new state: ${this.time.resolveHours()}:${this.time.resolveMinutes()}:${this.time.resolveSeconds()}.${this.time.resolveMilliseconds()}\n`);
        this.#storage.recoverPreviousState();
        console.log(`original state has been restored...`);
        this.displayTime();
    }
    ofElapsedTime() {
        const currentTime = {
            hours: 2,
            minutes: 1,
            seconds: 1,
            milliseconds: 1
        };
        const time = new Time(currentTime);
        let elapsedTime = CalculatorHelper
            .calculateElapsedTime(this.#timeState, currentTime);
        console.log(`current time: ${time.resolveHours()}:${time.resolveMinutes()}:${time.resolveSeconds()}.${time.resolveMilliseconds()}\n`);
        console.log(`elapsed time relative to the original time...`);
        console.log(`elapsed hours: ${elapsedTime.hours}\nelapsed minutes: ${elapsedTime.minutes}\nelapsed seconds:${elapsedTime.seconds}\nelapsed milliseconds: ${elapsedTime.milliseconds}`);
    }
    displayTime() {
        console.log(`state: ${this.time.resolveHours()}:${this.time.resolveMinutes()}:${this.time.resolveSeconds()}.${this.time.resolveMilliseconds()}`);
    }
}
const client = new Client;
console.log(`------------\n`);
client.displayTime();
client.ofConversion();
client.ofSum();
console.log('\n------------\n');
client.ofElapsedTime();
//# sourceMappingURL=index.mjs.map